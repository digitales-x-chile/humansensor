####### INSTALL ######
- Link Apache to your webclient location 
- Create a symbolic link (windows users use mklink command)

- Create folders for cache and compile
mkdir /tmp/compile
mkdir /tmp/cache
chmod 777 /tmp/compile
chmod 777 /tmp/cache

- Edit your local files 
    copy webclient/.htaccess.default to webclient/.htaccess
    copy webservice/src/config.php.default to webservice/src/config.php
    copy webservice/tests/config/config.php.default to webservice/tests/config/config.php
    
- To run the test go to the webservice/tests folder and run: python rest_human.py



On Windows

Requires:
	- Apache (XAMPP)
	- MySQL
	- PHP
	- Python 2.6
		- simplejson
Instructiones:
- Link Apache to your webclient location (Virtual Host)
- Create a symbolic link inside the webclient folder to your webservice folder (windows users use mklink command) (mklink /D webclient_folder/webservice webservice_folder)
- Create folders for cache and compile inside the webclient folder:
	webclient_folder/compile
	webclient_folder/tmp/cache
	webclient_folder/tmp/compile
	webclient_folder/tmp/cache
- Create a local copy of the database (if working offline)
	- Execute webservice\db\versions\1_upgrade.sql
- Edit your local files 
	copy webclient/.htaccess.default to webclient/.htaccess
	copy webservice/src/config.php.default to webservice/src/config.php
	copy webservice/tests/config/config.php.default to webservice/tests/config/config.php
- Check if everyone is working
	- http://localhost/service/entries
	- http://localhost/ (a form to add specific data)