<?php
require_once "../../config.php";
require_once("$ROOT/lib/smarty/Smarty.class.php");

class SmartyFactory{
	function getInstance(){
		$smarty = new Smarty();		

		$smarty->template_dir = "$GLOBALS[ROOT]/templates/";
		$smarty->compile_dir = $GLOBALS["SMARTY_COMPILE_PATH"];
		$smarty->cache_dir = $GLOBALS["SMARTY_CACHE_PATH"];
		$smarty->caching = false;	
		
		return $smarty;
	}
}
?>