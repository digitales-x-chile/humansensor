<?php
require_once "../../config.php";
require_once "$ROOT/model/persistence/Persistence.php";
require_once "$ROOT/model/resources/BaseResource.php";
require_once "$ROOT/utils/Utils.php";

class Tags extends BaseResource{
	
	static public function create($persistence,$params){
		$query = "INSERT INTO tags (entry_id, tag) VALUES (?, ?, ?, ?)";
		$queryParams = Array(
			$params['entry_id']
			,$params['tag']
		);
		$persistence->execute($query, $queryParams);
		return $persistence->execute("SELECT LAST_INSERT_ID()");
	}
}