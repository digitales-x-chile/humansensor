<?php
require_once "../../config.php";
require_once "$ROOT/model/persistence/Persistence.php";
require_once "$ROOT/model/resources/BaseResource.php";
require_once "$ROOT/utils/Utils.php";

class Entries extends BaseResource{
	
	static public function create($persistence,$params){
		$query = "INSERT INTO entries (url, message, client, lat, `long`, alt, address) VALUES (?, ?, ?, ?, ?, ?, ?)";
		$queryParams = Array(
			$params['url']
			,$params['message']
			,$params['client']
			,$params['latitude']
			,$params['longitude']
			,$params['altitude']
			,$params['address']
		);
		$persistence->execute($query, $queryParams);
		$queryId = "SELECT LAST_INSERT_ID() as id";
		$rows = Entries::queryGetAll($persistence, $queryId, Array());
		return $rows[0]["id"];
	}
	
	static public function update($persistence,$params){
		
		$query = "UPDATE entries SET url = ?, message = ?, client = ?, lat =?, `long`=?, alt=?, address=?, tags=? WHERE id = ?";
		$queryParams = Array(
			$params['url']
			,$params['message']
			,$params['client']
			,$params['lat']
			,$params['long']
			,$params['alt']
			,$params['address']
			,$params['tags']			
			,$params['entry_id']
		);
		$persistence->execute($query, $queryParams);
	}
	
	static public function show($persistence,$id){
		$query = "SELECT
			id
			,created
			,url
			,message
			,client
			,lat
			,`long`
			,alt
			,address
			,tags
			FROM entries
			where id = ?";
		return Entries::queryGetAll($persistence, $query, Array($id));
	}

	static public function showAll($persistence,$limit, $offset){
		$query = "SELECT
			id
			,created
			,url
			,message
			,client
			,lat as latitude
			,`long` as longitude
			,alt as altitude
			,address
			,tags
			FROM entries
			order by created desc limit ".Utils::dbEncode($limit)." offset ".Utils::dbEncode($offset);
		return Entries::queryGetAll($persistence, $query, Array());
	}
}
