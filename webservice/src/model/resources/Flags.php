<?php
require_once "../../config.php";
require_once "$ROOT/model/persistence/Persistence.php";
require_once "$ROOT/model/resources/BaseResource.php";
require_once "$ROOT/utils/Utils.php";

class Flags extends BaseResource{
	
	static public function create($persistence,$params){
		$query = "INSERT INTO flags (entry_id, ip, created_at, flag) VALUES (?, ?, ?, ?)";
		$queryParams = Array(
			$params['entry_id']
			,$params['ip']
			,$params['created_at']
			,$params['flag']
		);
		$persistence->execute($query, $queryParams);
		return $persistence->execute("SELECT LAST_INSERT_ID()");
	}
}