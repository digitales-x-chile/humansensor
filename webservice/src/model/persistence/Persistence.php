<?php
require_once "../../config.php";
require_once "$ROOT/utils/Utils.php";

class Persistence {
	protected static function connect(){
		$conn = mysql_connect($GLOBALS['DB_HOST'],$GLOBALS['DB_USER'],$GLOBALS['DB_PASSWORD']);
		if(!$conn){
			throw new Exception('Could not connect: ' . mysql_error());			
		}
		
		$db_selected = mysql_select_db($GLOBALS['DB_NAME'], $conn);
		if (!$db_selected) {
			throw new Exception("Can\'t use $db_name: " . mysql_error());
		}
		return $conn;
	}
	
	protected static $conn = null;
	
	public function Persistence(){
		if(!isset(Persistence::$conn)){
			$conn = Persistence::connect();
		}
		mysql_query("set autocommit = 1");
	}

	public function execute($query,$params = null){
		if($params){
			foreach($params as &$v){
				if(!is_null($v)){
					$v = "'".Utils::dbEncode($v)."'";
				}
				else{
					$v = "NULL";
				}
			}
			$query = vsprintf(str_replace("?","%s",$query),$params);
		}
		$rs = mysql_query($query);
		if (!$rs) {
			throw new Exception("Invalid query, error: " . mysql_error().", query: $query");
		}
		return $rs;
	}
	
	public function close(){
		mysql_close();
	}

}
?>
