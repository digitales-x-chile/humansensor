<?php
require_once "../../config.php";
require_once "$ROOT/model/persistence/Persistence.php";
require_once "$ROOT/model/resources/Entries.php";

try{
	
	if ((!isset($_POST['url']) && !isset($_POST['message'])) || ($_POST['url'] == null && $_POST['message'] == null)) {
		header("HTTP/1.0 400 Bad Request");
        	return;
	}
	$persistence = new Persistence();
	$idEntry = Entries::create($persistence,$_POST);
	$persistence->close();
	header("HTTP/1.0 201 Created");
	header("Location: $GLOBALS[HOST_URL]/service/entries/$idEntry");
}catch(Exception $e){
	header("HTTP/1.0 500 Server Internal Error");
	echo $exception;
}
