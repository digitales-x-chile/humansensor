<?php
require_once "../../config.php";
require_once "$ROOT/model/persistence/Persistence.php";
require_once "$ROOT/model/resources/Entries.php";
require_once "$ROOT/model/resources/Tags.php";

try{
	$id = $_GET["id"];
	$persistence = new Persistence();
	$_POST['entry_id'] = $id;
	Entries::update($persistence,$_POST);
	$persistence->close();
	header("HTTP/1.0 204 No Content");
}catch(Exception $e){
	header("HTTP/1.0 500 Server Internal Error");
	echo $e;
}
