<?php
require_once "../../config.php";
require_once "$ROOT/model/persistence/Persistence.php";
require_once "$ROOT/model/resources/Entries.php";

try{
	$id = $_GET["id"];
	$persistence = new Persistence();
	$entries = Entries::show($persistence,$id);
	$persistence->close();
	header("HTTP/1.0 200 Ok");
	header("Content-Type: text/json", true);
	echo json_encode($entries[0]);
}catch(Exception $e){
	header("HTTP/1.0 500 Server Internal Error");
	echo $exception;
}