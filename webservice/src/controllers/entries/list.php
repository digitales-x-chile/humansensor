<?php
require_once "../../config.php";
require_once "$ROOT/model/persistence/Persistence.php";
require_once "$ROOT/model/resources/Entries.php";

try{
	$offset = isset($_GET["offset"]) ? $_GET["offset"] : 0;
	$limit = $GLOBALS["RESPONSE_LIMIT"];
	$persistence = new Persistence();
	$entries = Entries::showAll($persistence,$limit, $offset);
	$persistence->close();
	header("HTTP/1.0 200 Ok");
	header("Content-Type: text/json", true);
	echo json_encode($entries);
}catch(Exception $e){
	header("HTTP/1.0 500 Server Internal Error");
	echo $exception;
}