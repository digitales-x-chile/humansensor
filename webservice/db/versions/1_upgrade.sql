DROP SCHEMA IF EXISTS `humansensor` ;
CREATE SCHEMA IF NOT EXISTS `humansensor` DEFAULT CHARACTER SET utf8 ;

DROP TABLE IF EXISTS `humansensor`.`schema_migrations` ;

CREATE TABLE IF NOT EXISTS `humansensor`.`schema_migrations` (
	`version` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`version`)	);

INSERT INTO `humansensor`.`schema_migrations` VALUES ("1");

DROP TABLE IF EXISTS `humansensor`.`entries` ;

CREATE  TABLE IF NOT EXISTS `humansensor`.`entries` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`ip` VARCHAR(39) NOT NULL ,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`url` VARCHAR(255) NULL ,
	`message` TEXT NULL ,
	`client` VARCHAR(255) NULL ,
	`lat` FLOAT NULL ,
	`long` FLOAT NULL ,
	`alt` FLOAT NULL ,
	PRIMARY KEY (`id`)
) TYPE=innodb default charset=utf8;
