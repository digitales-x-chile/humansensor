#!/usr/bin/python
import sys
sys.path.append("libs")
from restful_lib import Connection 
import simplejson as json
import unittest
from urllib import urlencode
from config.config import baseurl

N = 10

class TestREST(unittest.TestCase):
    def setUp(self):
        self.conn = Connection(baseurl)
      
    def test_post_success(self):
        for i in xrange(N):
            args = {'url':'url%d' %(i,), 'message': 'msg%d' %(i,), 'latitude': i-50.1, 'longitude': i*3.6-180, 'altitude': 570, 'location': 'loc%d' %(i,), 'address':'Santiago, Chile', 'tags':''}
            self.post_success(args)
    
    def test_list(self):
        res = self.conn.request_get('entries/')
        self.assertEqual('200', res['headers']['status'])

    def post_success(self, args):
        args['client'] = 'test_client'
        res = self.conn.request_post('entries/', body = urlencode(args), headers = {"Content-Type": "application/x-www-form-urlencoded"})
        self.assertEqual('201', res['headers']['status'])
        self.assertTrue(res['headers'].has_key('location'))
        self.get(res['headers']['location'], args)
        

    def get(self, url, dict):
        res = self.conn.request_get(url.replace(baseurl,""), headers={'Accept':'text/json'})
        self.assertEqual('200',res['headers']['status'])
        obj = json.loads(res['body'])
        obj["latitude"] = float(obj["latitude"])
        obj["longitude"] = float(obj["longitude"])
        obj["altitude"] = float(obj["altitude"])
        
        dict["latitude"] = float(dict["latitude"])
        dict["longitude"] = float(dict["longitude"])
        dict["altitude"] = float(dict["altitude"])
        del obj["created"]
        del obj["id"]
        del dict["location"]
        try:
            del dict["created"]
        except:
            pass
        self.assertEqual(dict,obj)

    def test_post_fail(self):
        for i in xrange(N):
            args = {'latitude': i-50.1, 'longitude': i*3.6-180, 'altitude': 570, 'location': 'loc%d' %(i,)}
            self.post_fail(args)

    def post_fail(self, args):
        args['client'] = 'test_client'
        res = self.conn.request_post('entries/', body = urlencode(args), headers = {"Content-Type": "application/x-www-form-urlencoded"})
        self.assertEqual('400',res['headers']['status'])

    def test_post_flag(self):
        args = {}
        args['flag'] = 3
        res = self.conn.request_post('flags/create/1', body = urlencode(args), headers = {"Content-Type": "application/x-www-form-urlencoded"})
        self.assertEqual('201', res['headers']['status'])
    
    def test_update(self):
        res = self.conn.request_get('entries/')
        self.assertEqual('200', res['headers']['status'])
        list = json.loads(res["body"])
        for entry in list:
            self.update(entry)
            
    def update(self,obj):
        args = dict(obj)
        for k in args.keys():
            if type(args[k]) == str or type(args[k]) == unicode:
                args[k] = args[k] + "10"
        args["location"]=""
        del args["id"]
        res = self.conn.request_post('entries/update/%s' %(obj["id"],), body = urlencode(args), headers = {"Content-Type": "application/x-www-form-urlencoded"})
        self.assertEqual('204',res['headers']['status'])
        self.get("entries/%s" %(obj["id"],),args)    
        
if __name__ == '__main__':
    import sys
    unittest.main()
