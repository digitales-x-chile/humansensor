# -*- coding: utf-8 -*-
#!/usr/bin/python
import sys
sys.path.append("libs")
import twython.core as twython
from restful_lib import Connection 
import simplejson as json
from urllib import urlencode
import time
import config
import re

conn = Connection(config.baseurl)
twitter = twython.setup(username=config.user,password=config.passw)
re_url = re.compile("http://[^\s]+")

def upload_results(results):
    for obj in results:
        url = (re_url.findall(obj["text"])+[""])[0]
        args = {
            'url': url,
            'message': obj['text'].encode("utf-8"),
            'client': 'twitter@%s:%s' %(obj["from_user"],obj['id'])
        }
        res = conn.request_post('entries', body = urlencode(args), headers = {"Content-Type": "application/x-www-form-urlencoded"})
        if res["headers"]["status"] == '201':
            #print res
            reply_url = res["headers"]["location"]
            reply_url = reply_url.replace("entries/","update_entries/")
            reply_msg = "@%s Could you give us further information by filling %s" %(obj["from_user"], reply_url)
            print reply_msg
            print obj["id"]
            print twitter.updateStatus(reply_msg, in_reply_to_status_id=obj["id"])
        else:
            print "Error: uploading entry"
def main():
    print "fetching...",
    results = twitter.searchTwitter(config.query, rpp=config.limit)
    print len(results['results']), 'results found'
    last_id = results['max_id']
    upload_results(results['results'])
    while True:
        time.sleep(config.sleep_time)
        print "fetching"
        results = twitter.searchTwitter(config.query, since_id=last_id, rpp=config.limit)
        print len(results['results']), 'results found'
        last_id = results['max_id']
        upload_results(results['results'])
    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print "Exit"
        
